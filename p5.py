import pygame
from math import sin,pi,cos,tan
from math import copysign as sign

rad = lambda z:z/57.2957795

inp = open('input.txt', 'r')






BLACK = ( 0, 0, 0)
WHITE = ( 255, 255, 255)
GREEN = ( 0, 255, 0)
RED = ( 255, 0, 0)
BLUE=(0,77,225)
pygame.init()


w,h=1400, 850

size = (w, h)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("My Game")
done = False
clock = pygame.time.Clock()



x0=w/2
y0=h/2



exec("R="+inp.readline() )

exec("g="+inp.readline() )

exec("l="+inp.readline() )

exec("phi0="+inp.readline() )

#phi0+=180

phi0=rad(phi0)

exec("time_constant="+inp.readline())

exec("tx="+inp.readline() )

exec("prec="+inp.readline())




t0=0
dt=0.05
dt_up=0.025
dt_down=0.001


xb=x0
yb=y0

## begin calc


def coordinates(t):
	xb=x0+R*cos(t*time_constant)
	yb=y0+R*sin(t*time_constant)
	return (xb,yb)



def d2(t):
	precision=dt_down
	c_m=coordinates(t-precision)
	c_0=coordinates(t)
	c_p=coordinates(t+precision)

	ddx=( c_p[0]+c_m[0]-2*c_0[0] )/(precision**2)
	ddy=( c_p[1]+c_m[1]-2*c_0[1] )/(precision**2)

	#return (ddx,0)
	return ( ddx,ddy )

def function(t,dtheta,theta):
	dd=d2(t)

	return -( (dd[1]-g)*sin(theta) - (dd[0])*cos(theta) )/(l)


table=[] #time  dtheta theta

table.append((t0,0,phi0))

curr_t=t0
while curr_t<tx:

	dtheta_full=table[-1][1]+dt*function(* table[-1])
	theta_full=table[-1][2]+dtheta_full*dt

	t_half=table[-1][0]+0.5*dt

	dtheta_half_a=table[-1][1]+0.5*dt*function(* table[-1])
	theta_half_a=table[-1][2]+dtheta_half_a*dt*0.5

	stack=(t_half,dtheta_half_a,theta_half_a)

	dtheta_half_b=stack[1]+0.5*dt*function(* stack)
	theta_half_b=stack[2]+dtheta_half_b*dt*0.5


	diff=abs(theta_full- theta_half_b)

	comeon=True

	if diff>prec and dt >dt_down:
		dt/=2.0
		comeon=False
		#print "decrease"
	if diff<prec and dt<dt_up:
		dt*=2.0
		#print "increase"

	if comeon:
		curr_t+=dt


		table.append((curr_t,dtheta_half_b,theta_half_b))


print (len(table))

xtrace=[]
ytrace=[]

i=0

def dr(t,i):

	while i<len(table) and i>=0 and table[i][0]<t:
		i+=1

	while i>=0 and i<len(table) and table[i][0]>t:
		i-=1


	if i<=0:
		return (i,table[0][2])
	if i>=len(table)-1:
		return (i,table[-1][2])

	j=0
	k=0

	if table[i][0]==t:
		return (i,table[i][2])
	elif table[i][0]>t:
		j=i
		k=i+1
	elif table[i][0]<t:
		k=i
		j=i-1

	return (i,table[j][2]+(t-table[j][0])*(table[k][2]-table[j][2])/(table[k][0]-table[j][0]))

curr_t=t0

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
	screen.fill(BLACK)


	if curr_t<tx:
		curr_t+=0.02
		#print curr_t


	c=coordinates(curr_t)

	xb=c[0]
	yb=c[1]

	curr_theta=dr(curr_t,i)

	i=curr_theta[0]
	curr_theta=curr_theta[1]


	x=xb+l*sin(curr_theta)

	y=yb+l*cos(curr_theta)

	x=int(x)
	y=int(y)

	xb=int(xb)
	yb=int(yb)

	#pygame.draw.circle(screen,WHITE,[45,45],5,5)

	pygame.draw.circle(screen,WHITE,[int(xb),h-int(yb)],5,1)

	pygame.draw.circle(screen,BLUE,[int(x0),int(y0)],R,1)

	pygame.draw.circle(screen,WHITE,[x,h-y],20,2)
	pygame.draw.aaline(screen, WHITE, [xb,h-yb], [x,h-y], 1)

	xtrace.append(x)
	ytrace.append(h-y)

	for j in range(len(xtrace)):
		pygame.draw.circle(screen,GREEN,[xtrace[j],ytrace[j]],1,1)


	pygame.display.flip()
	clock.tick(100)

pygame.quit()
